

const express = require('express');
const app = express();
const {done, notfound} = require('../../services/response')
const {sendMessage, register, list, remove} = require('./controllers')

//Routes Specified For Device
app.post("/sendMessage", async (req, res) => { sendMessage(req, res) });
app.post("/register", async (req, res) => register(req, res));
app.get("/list", async (req, res)=> list(req, res));
app.delete("/delete/:id", async (req, res)=> remove(req, res));

module.exports = app