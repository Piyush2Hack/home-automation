const mongoose = require('mongoose');

const DeviceSchema = new mongoose.Schema({
  name: {type: String,required: true,trim: true,lowercase: true},
  ip: {type: String,required: true,trim: true,lowercase: true}
});

const Device = mongoose.model("Device", DeviceSchema);
module.exports = Device;