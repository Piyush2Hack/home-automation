const DeviceModel = require('../models/Device');
var mqttHandler = require('../../../services/mqtt/MqttHandler');
//Mqtt for Listening  Publising and  Subscribing messages
const mqttClient = new mqttHandler({TOPIC}=process.env);
mqttClient.connect();

module.exports.sendMessage = (req, res) => {
	const {message} = req.body;
    mqttClient.sendMessage(message);
    res.status(200).send({message:"Sent"});
}

module.exports.register = async (req, res) => {
	const device = new DeviceModel(req.body);
    try {
        await device.save();
        res.status(200).send({message:"Device Registered"});
    }catch (err) {
        res.status(500).send(err);
    }
}

module.exports.list = async (req, res) => {
	const devices = await DeviceModel.find({});
    try {
        res.status(200).send(devices);
    } catch (err) {
        res.status(500).send(err);
    }
}

module.exports.remove = async (req, res) => {
	try {
        console.log(req.params)
        const device = await DeviceModel.findByIdAndDelete(req.params.id)
        if (!device) res.status(404).send("No item found")
        res.status(200).send({message:"Deleted"})
      } catch (err) {
        res.status(500).send(err)
    }
}

