module.exports.done = (res, {status, entity}) => {
	if (entity) {
		res.status(status || 200).json(entity)
	}
	res.status(status).end()
}

module.exports.notFound  = (res, {status, entity}) => {
	if (entity) {
		res.status(status || 200).json(entity)
	}
	res.status(status).end()
}

