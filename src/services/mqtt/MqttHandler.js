const mqtt = require('mqtt');

class MqttHandler {
  constructor(topic) {
    this.mqttClient = null;
    this.host = 'mqtt://test.mosquitto.org';
    this.topic = topic;
  }
  
  connect(topicName) {
    // Connect mqtt with credentials
    this.mqttClient = mqtt.connect(this.host);

    // Mqtt error calback
    this.mqttClient.on('error', (err) => {
      console.log(err);
      this.mqttClient.end();
    });

    // Connection callback
    this.mqttClient.on('connect', () => {
      console.log(`mqtt client connected`);
    });

    // mqtt subscriptions
    this.mqttClient.subscribe(process.env.TOPIC, {qos: 1});

    // When a message arrives, console.log it
    this.mqttClient.on('message', function (topic, message) {
      console.log(message.toString());
    });

    this.mqttClient.on('close', () => {
      console.log(`mqtt client disconnected`);
    });
  }

  // Sends a mqtt message to topic
  sendMessage(message) {
    this.mqttClient.publish(process.env.TOPIC, message);
  }
}

module.exports = MqttHandler;