const jwt  = require('jsonwebtoken')

const auth = async (req,res,next) => {
    try {
        const token = req.header('Authorization').replace('Bearer', '').trim()
        const decoded  = jwt.verify(token, process.env.JWT_SECRET)
        console.log(decoded);
        req.token = token
        next()
    } catch (error) {
        console.log(error)
        res.status(401).send({error:'Please authenticate with Chingari!'})
    }
}

module.exports = auth
