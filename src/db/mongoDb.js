const mongoose = require('mongoose');
let isConnected;

const connectDatabase = async function(){
    await mongoose.connect( process.env.MONGODB_URL,{ useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true });
}   

module.exports = connectDatabase;