Home Automation Using  Node, MQTT Broker and Sending Messages to Raspberry PIE

Prerequiste:
A Laptop with Node Installed
Raspberry Pie
BreadBoard
Display To connect to Raspberry Pie.

Command to Start Server:
npm run serve

<b>WEB APP TO CONTROL DEVICE<b>:
https://chingari.netlify.app/


<b>Authorization Token<b> in Each Request to be Used in Headers:

headers:{
    'Authorization':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiY2hpbmdhcmkiLCJpYXQiOjE1OTY1OTAyNjAsImV4cCI6MTU5NjY5ODI2MH0.J7a_-Gkxkm5rv2y4PTscqzAO8_r6t8UdBy6U7Ed_GZY' 
}

<b>API ENDPOINTS<b>:

ADD DEVICE:
http://localhost:3000/device/register

{
    "name":"Test Device",
    "ip":"192.182.12.1"
}

--------------------------------------------------

SEND MESSAGE:
http://localhost:3000/device/sendMessage

Body:
{
    "message":"1"
}

--------------------------------------------------

DELETE DEVICE:
http://localhost:3000/device/delete/{deviceID}

--------------------------------------------------

LIST DEVICE:
http://localhost:3000/device/list


TEST MQTT BROKER SERVER:
mqtt://test.mosquitto.org

RUNNING A PROJECT:
npm run serve
