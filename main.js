var express = require("express");
var bodyParser = require("body-parser");
var app = express();
require('dotenv').config()
var cors = require('cors')
const auhHandler = require('./src/services/middlewares/auth');

//Registering Routes
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

app.use(cors())
//Authenticate the Request
app.use(auhHandler)
app.use('/device',require('./src/api/device'));

setImmediate(async () => {
    try{
        await require('./src/db/mongoDb')();
        console.log('Connection to DB Successful');
        const {PORT} = process.env;
        var server = app.listen(PORT, function () {
            console.log("app running on port.", server.address().port);
        });        
    }catch(err){
        console.log('Connection to DB Failed');
        throw new Error('Failed Connection to DB');
    }
})

